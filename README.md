# Тестовое задание Telros 

## Описание задачи
Ссылка https://disk.yandex.ru/i/5YYd9OcSo418Tw

## Как работать
В переменных среды IntelleJ IDEA прописать данные своей локальной базы данных Postgres, наименования переменных есть в файле application.properties . Так же добавить ключи доступа к Amazon S3

## Используемые технологии
* Java 17
* Spring Boot
* Maven
* Rest API
* Spring Data JPA
* PostgreSQL
* Swagger
* Amazon S3
* Spring Boot Security
* JWT