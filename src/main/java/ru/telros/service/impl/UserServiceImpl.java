package ru.telros.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.telros.dto.SignUpForm;
import ru.telros.dto.UserDto;
import ru.telros.dto.UserResponse;
import ru.telros.model.Role;
import ru.telros.model.Status;
import ru.telros.model.User;
import ru.telros.repository.UserRepository;
import ru.telros.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponse save(SignUpForm form) {
        if (userRepository.findByUsername(form.getUsername()).isPresent()){
            throw new IllegalArgumentException("Пользователь с таким username уже зарегистрирован!");
        }

        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .middleName(form.getMiddleName())
                .birthDate(form.getBirthDate())
                .username(form.getUsername())
                .password(passwordEncoder.encode(form.getPassword()))
                .status(Status.ACTIVE)
                .build();

        //Добавляем роль для пользователя
        if (form.getRole() == null){
            form.setRole(Role.USER);
        }
        user.setRole(form.getRole());
        userRepository.save(user);

        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .middleName(user.getMiddleName())
                .birthDate(user.getBirthDate())
                .role(user.getRole())
                .build();
    }

    @Override
    public List<UserDto> findAll() {
        return UserDto.from(userRepository.findAllByStatusEquals(Status.ACTIVE));
    }

    @Override
    public UserDto getById(Integer userId) {
        return UserDto.from(userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist")));
    }

    @Override
    public UserDto update(Integer userId, UserDto userDto) {
        if (isUserExist(userId)) {
            User user = User.builder()
                    .id(userDto.getId())
                    .firstName(userDto.getFirstName())
                    .lastName(userDto.getLastName())
                    .middleName(userDto.getMiddleName())
                    .birthDate(userDto.getBirthDate())
                    .status(userDto.getStatus())
                    .build();
            return UserDto.from(userRepository.save(user));
        } else {
            throw new IllegalArgumentException("User with this id doesn't exist");
        }
    }

    @Override
    public void delete(Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        user.setStatus(Status.DELETED);
        userRepository.save(user);
    }

    @Override
    public boolean isUserExist(Integer userId) {
        return userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId).isPresent();
    }

    public boolean isFileOwnedByUser(Integer userId, Integer fileId) {
        try {
            User user = userRepository
                    .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                    .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
            return user.getPhotos().stream()
                    .filter(x -> x.getStatus().equals(Status.ACTIVE))
                    .anyMatch(y -> y.getId().equals(fileId));
        } catch (IllegalArgumentException e){
            //в контроллере уже Response обрабатывает в зависимости от false/true в данном методе
            return false;
        }
    }

    public boolean isFileWithThisNameAlreadyExist(MultipartFile file, Integer userId) {
        User user = userRepository
                .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        return user.getPhotos().stream().anyMatch(photo -> photo.getName().equals(file.getOriginalFilename()));
    }
}
