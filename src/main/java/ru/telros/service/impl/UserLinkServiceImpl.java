package ru.telros.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.telros.dto.UserLinkDto;
import ru.telros.model.Status;
import ru.telros.model.User;
import ru.telros.model.UserLink;
import ru.telros.repository.UserLinkRepository;
import ru.telros.repository.UserRepository;
import ru.telros.service.UserLinkService;
import ru.telros.service.UserService;

@Service
@RequiredArgsConstructor
public class UserLinkServiceImpl implements UserLinkService {

    private final UserLinkRepository userLinkRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    @Override
    public UserLinkDto getById(Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        return UserLinkDto.from(user.getUserLink());
    }

    @Override
    public UserLinkDto save(UserLinkDto userLinkDto, Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        UserLink userLink = UserLink.builder()
                .email(userLinkDto.getEmail())
                .phoneNumber(userLinkDto.getPhoneNumber())
                .build();
        userLinkRepository.save(userLink);
        user.setUserLink(userLink);
        userRepository.save(user);
        return UserLinkDto.from(userLink);
    }

    @Override
    public UserLinkDto update(UserLinkDto userLinkDto, Integer linkId, Integer userId) {
        if (!userService.isUserExist(userId)) {
            throw new IllegalArgumentException("User with this id doesn't exist");
        }
        if (isUserLinkExist(userLinkDto.getId())) {
            UserLink userLink = UserLink.builder()
                    .id(linkId)
                    .email(userLinkDto.getEmail())
                    .phoneNumber(userLinkDto.getPhoneNumber())
                    .build();
            return UserLinkDto.from(userLinkRepository.save(userLink));
        } else {
            throw new IllegalArgumentException("User link with this id doesn't exist");
        }
    }

    @Override
    public boolean isUserLinkExist(Integer userLinkId) {
        return userLinkRepository.findById(userLinkId).isPresent();
    }

    @Override
    public void delete(Integer linkId, Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        if (user.getUserLink().getId().equals(linkId)){
            user.setUserLink(null);
            userLinkRepository.deleteById(linkId);
        } else {
            throw new IllegalArgumentException("User link with this id doesn't below to user");
        }
    }
}
