package ru.telros.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.telros.dto.PhotoFileDto;
import ru.telros.model.PhotoFile;
import ru.telros.model.Status;
import ru.telros.model.User;
import ru.telros.repository.PhotoFileRepository;
import ru.telros.repository.UserRepository;
import ru.telros.util.FileUtil;
import ru.telros.service.PhotoFileService;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PhotoFileServiceImpl implements PhotoFileService {
    private final PhotoFileRepository photoFileRepository;
   private final S3Service s3Service;
   private final UserRepository userRepository;

    public List<PhotoFileDto> findAll() {
        return PhotoFileDto.from(photoFileRepository.findAll());
    }

    public PhotoFileDto save(MultipartFile file, Integer userId) {
        String location;
        //проверка юзера в контроллере
        User user = userRepository.findById(userId).get();

        try {
            java.io.File physicalFile = FileUtil.multipartToFile(file, file.getOriginalFilename());
            location = "id" + user.getId() + "/" + file.getOriginalFilename();
            s3Service.putS3Object(location, physicalFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        PhotoFile fileData = PhotoFile.builder()
                .name(file.getOriginalFilename())
                .location(location)
                .status(Status.ACTIVE)
                .user(user)
                .build();
        PhotoFile createdFile = photoFileRepository.save(fileData);
        return PhotoFileDto.from(createdFile);
    }

    public PhotoFileDto getById(Integer fileId){
        return PhotoFileDto.from(photoFileRepository.findById(fileId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist")));
    }

    public PhotoFileDto update(Integer fileDtoId, MultipartFile file, Integer userId) {
        PhotoFile existedFile = photoFileRepository.findById(fileDtoId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist"));
        String location;
        try {
            java.io.File physicalFile = FileUtil.multipartToFile(file, existedFile.getName());
            location = "id" + userId + "/" + file.getOriginalFilename();
            s3Service.putS3Object(location, physicalFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!file.getOriginalFilename().equals(existedFile.getName())){
            //нужно удалять файл из s3 так как при смене имени файла амазон добавляет новый файл, а старый там так и остается уже не нужный
            s3Service.deleteS3Object(existedFile.getLocation());
        }
        existedFile.setName(file.getOriginalFilename());
        existedFile.setLocation(location);
        return PhotoFileDto.from(photoFileRepository.save(existedFile));
    }

    public void delete(Integer fileDtoId, Integer userId) {
        PhotoFile existedFile = photoFileRepository.findById(fileDtoId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist"));
        s3Service.deleteS3Object(existedFile.getLocation());
        existedFile.setStatus(Status.DELETED);
        photoFileRepository.save(existedFile);
    }
}
