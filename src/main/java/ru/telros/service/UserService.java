package ru.telros.service;

import org.springframework.web.multipart.MultipartFile;
import ru.telros.dto.SignUpForm;
import ru.telros.dto.UserDto;
import ru.telros.dto.UserResponse;

import java.util.List;

public interface UserService {
    UserResponse save(SignUpForm form);

    List<UserDto> findAll();

    UserDto getById(Integer userId);

    boolean isUserExist(Integer userId);

    UserDto update(Integer userId, UserDto userDto);

    void delete(Integer userId);
    boolean isFileOwnedByUser(Integer userId, Integer fileId);

    public boolean isFileWithThisNameAlreadyExist(MultipartFile file, Integer userId);
}
