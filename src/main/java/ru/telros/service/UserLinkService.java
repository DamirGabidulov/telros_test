package ru.telros.service;

import ru.telros.dto.UserLinkDto;

public interface UserLinkService {
    UserLinkDto getById(Integer userId);

    UserLinkDto save(UserLinkDto userLinkDto, Integer userId);

    UserLinkDto update(UserLinkDto userLinkDto, Integer linkId, Integer userId);

    boolean isUserLinkExist(Integer userLinkId);

    void delete(Integer linkId, Integer userId);
}
