package ru.telros.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ru.telros.model.PhotoFile;
import ru.telros.model.Role;
import ru.telros.model.Status;
import ru.telros.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    @JsonIgnore
    private List<PhotoFile> photos;
    private Status status;
    private Role role;
    private String username;

    public static UserDto from(User user){
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .middleName(user.getMiddleName())
                .birthDate(user.getBirthDate())
                .photos(user.getPhotos())
                .status(user.getStatus())
                .role(user.getRole())
                .username(user.getUsername())
                .build();
    }

    public static List<UserDto> from(List<User> users){
        return users.stream().map(UserDto::from).collect(Collectors.toList());
    }
}
