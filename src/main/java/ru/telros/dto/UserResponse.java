package ru.telros.dto;

import lombok.Builder;
import lombok.Data;
import ru.telros.model.Role;

import java.time.LocalDate;

@Data
@Builder
public class UserResponse {
    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private Role role;
}
