package ru.telros.dto;

import lombok.Builder;
import lombok.Data;
import ru.telros.model.UserLink;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class UserLinkDto {
    private Integer id;
    @NotBlank
    private String email;
    @NotBlank
    private Long phoneNumber;

    public static UserLinkDto from(UserLink userLink){
        return UserLinkDto.builder()
                .id(userLink.getId())
                .email(userLink.getEmail())
                .phoneNumber(userLink.getPhoneNumber())
                .build();
    }
}
