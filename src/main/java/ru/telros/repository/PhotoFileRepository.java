package ru.telros.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.telros.model.PhotoFile;

public interface PhotoFileRepository extends JpaRepository<PhotoFile, Integer> {
}
