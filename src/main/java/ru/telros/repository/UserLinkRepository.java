package ru.telros.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.telros.model.UserLink;

import java.util.Optional;

public interface UserLinkRepository extends JpaRepository<UserLink, Integer> {
//    Optional<UserLink> findById(Integer id);
}
