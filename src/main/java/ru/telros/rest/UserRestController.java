package ru.telros.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.telros.dto.SignUpForm;
import ru.telros.dto.UserDto;
import ru.telros.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
@Tag(name = "UserRestController", description = "Контроллер взаимодействия с данными пользователя")
public class UserRestController {

    private final UserService userService;

    @Operation(summary = "Получить всех пользователей, доступно только админу")
    @GetMapping
    public ResponseEntity<List<UserDto>> findAll() {
        List<UserDto> users = userService.findAll();
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return ResponseEntity.ok().body(users);
        }
    }

    @Operation(summary = "Получить пользователя по id")
    @GetMapping("/{userId}")
    public ResponseEntity<?> getById(@Parameter(description = "id юзера") @PathVariable("userId") Integer userId) {
        try {
            return ResponseEntity.ok().body(userService.getById(userId));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @Operation(summary = "Регистрация")
    @PostMapping("/signUp")
    public ResponseEntity<?> signUp(@Parameter(description = "форма регистрации") @RequestBody SignUpForm form) {
        try {
            return ResponseEntity.status(201).body(userService.save(form));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @Operation(summary = "Обновить пользователя по id")
    @PutMapping("/{userId}")
    public ResponseEntity<?> update(@Parameter(description = "id юзера") @PathVariable("userId") Integer userId,
                                    @Parameter(description = "форма данных юзера, которые нужно обновить") @RequestBody UserDto userDto) {
        if (userService.isUserExist(userId)) {
            return ResponseEntity.ok().body(userService.update(userId, userDto));
        } else {
            return new ResponseEntity<>("User with this id doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Удалить пользователя по id")
    @DeleteMapping("/{userId}")
    public ResponseEntity<?> delete(@Parameter(description = "id юзера") @PathVariable("userId") Integer userId) {
        if (userService.isUserExist(userId)) {
            userService.delete(userId);
            return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User with this id doesn't exist", HttpStatus.NOT_FOUND);
        }
    }
}
