package ru.telros.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.telros.dto.SignUpForm;
import ru.telros.dto.UserLinkDto;
import ru.telros.service.UserLinkService;
import ru.telros.service.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/links")
@Tag(name = "UserLinkRestController", description = "Контроллер взаимодействия с контактными данными пользователя")
public class UserLinkRestController {

    private final UserLinkService userLinkService;

    @Operation(summary = "Получить контактные данные пользователя с данным id")
    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getByUserId(@Parameter(description = "id юзера") @PathVariable("userId") Integer userId) {
        try {
            return ResponseEntity.ok().body(userLinkService.getById(userId));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @Operation(summary = "Сохранить контактные данные пользователя")
    @PostMapping()
    public ResponseEntity<?> save(@Parameter(description = "форма контактных данных юзера") @RequestBody UserLinkDto userLinkDto,
                                  @Parameter(description = "id юзера") @RequestHeader Integer userId) {
        try {
            return ResponseEntity.status(201).body(userLinkService.save(userLinkDto, userId));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @Operation(summary = "Обновить контактные данные пользователя")
    @PutMapping("/{linkId}")
    public ResponseEntity<?> update(@Parameter(description = "id контактных данных юзера") @PathVariable Integer linkId,
                                    @Parameter(description = "форма контактных данных юзера") @RequestBody UserLinkDto userLinkDto,
                                    @Parameter(description = "id юзера") @RequestHeader Integer userId) {
        try {
            return ResponseEntity.status(201).body(userLinkService.update(userLinkDto, linkId, userId));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @Operation(summary = "Удалить контактные данные пользователя")
    @DeleteMapping("/{linkId}")
    public ResponseEntity<?> delete(@Parameter(description = "id контактных данных юзера") @PathVariable Integer linkId,
                                    @Parameter(description = "id юзера") @RequestHeader Integer userId){
        try {
            userLinkService.delete(linkId, userId);
            return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
        }catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }
}
