package ru.telros.model;

public enum Status {
    ACTIVE, DELETED;
}
