package ru.telros.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;
import ru.telros.model.User;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class JwtProvider {
    public static final String SECRET_CODE = "secret_key_01";
    public static final Integer EXPIRED_TIME_MINUTE = 60 * 24;

    public static String generateToken(User user) {
        Date date = Date.from(LocalDateTime
                .now()
                .plusMinutes(EXPIRED_TIME_MINUTE)
                .atZone(ZoneId.systemDefault())
                .toInstant());
        return JWT.create()
                .withExpiresAt(date)
                .withSubject(user.getId().toString())
                .withClaim("username", user.getUsername())
                .withClaim("role", user.getRole().name())
                .withClaim("status", user.getStatus().name())
                .sign(Algorithm.HMAC256(SECRET_CODE));
    }

    public static String getLoginFromToken(String token) {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SECRET_CODE))
                    .build()
                    .verify(token);
            return decodedJWT.getClaim("username").asString();
    }

}
